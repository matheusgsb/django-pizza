orders = []


def print_orders():
    if orders:
        print '\n- Orders:\n'
        print('-'*40)
    for order in orders:
        s = 'Name: {} \nPizza: {}'
        print(s.format(order['name'], order['pizza']))
        if order['comments']:
            obs = 'Obs: {comments}'.format(**order)
            print(obs)
        print('-'*40)


def create_new_order(name, pizza, comments=None):

    new_order = {}
    new_order['name'] = name
    new_order['pizza'] = pizza
    new_order['comments'] = comments

    return new_order

orders.append(create_new_order('Mario', 'Pepperoni'))
orders.append(create_new_order('Marco', 'Ham', 'No onions'))
print_orders()
