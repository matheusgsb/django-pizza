orders = [
    {
        'name': 'Mario',
        'pizza': 'Pepperoni'
    },
    {
        'name': 'Marco',
        'pizza': 'Ham'
    }
]

for order in orders:
    s = '\nName: {} \nPizza: {}'
    print(s.format(order['name'],order['pizza']))



